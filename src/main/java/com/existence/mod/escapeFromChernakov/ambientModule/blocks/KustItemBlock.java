package com.existence.mod.escapeFromChernakov.ambientModule.blocks;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class KustItemBlock
        extends ItemBlock {
    public KustItemBlock(Block of) {
        super(of);
        this.setHasSubtypes(true);
    }

    public IIcon getIconFromDamage(int par1) {
        return BlockCustomKust.kustRegistry.get(par1).getIcon();
    }

    public int getMetadata(int par1) {
        return par1;
    }

    public String getItemStackDisplayName(ItemStack stack) {
        return BlockCustomKust.getFromRegistry((int) stack.getItemDamage()).name;
    }
}

