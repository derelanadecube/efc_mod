package com.existence.mod.escapeFromChernakov.ambientModule.bushes;

import cpw.mods.fml.common.SidedProxy;

public class KustProxy {
    @SidedProxy(clientSide = "com.existence.mod.escapeFromChernakov.ambientModule.bushes.KustClientProxy", serverSide = "com.existence.mod.escapeFromChernakov.ambientModule.bushes.KustProxy")
    public static KustProxy instance;
    public static int kustRenderId;

    public void registerTileRenders() {
    }
}
