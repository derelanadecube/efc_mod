package com.existence.mod.escapeFromChernakov.ambientModule.bushes;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

public class KustRenderer
        implements ISimpleBlockRenderingHandler {
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {
    }

    private void automaticTessel(float x, float y, float z, float ex, float ey, float ez, double u1, double u2, double v1, double v2, Tessellator tessellator) {
        tessellator.addVertexWithUV((double) x, (double) y, (double) z, u1, v2);
        tessellator.addVertexWithUV((double) x, (double) ey, (double) z, u1, v1);
        tessellator.addVertexWithUV((double) ex, (double) ey, (double) ez, u2, v1);
        tessellator.addVertexWithUV((double) ex, (double) y, (double) ez, u2, v2);
        tessellator.addVertexWithUV((double) ex, (double) y, (double) ez, u1, v2);
        tessellator.addVertexWithUV((double) ex, (double) ey, (double) ez, u1, v1);
        tessellator.addVertexWithUV((double) x, (double) ey, (double) z, u2, v1);
        tessellator.addVertexWithUV((double) x, (double) y, (double) z, u2, v2);
    }

    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        Tessellator tessellator = Tessellator.instance;
        tessellator.setBrightness(block.getMixedBrightnessForBlock(world, x, y, z));
        int md = world.getBlockMetadata(x, y, z);
        KustObject ko = BlockCustomKust.getFromRegistry(md);
        IIcon icon = ko.getIcon();
        float f = 1.0f;
        int l = block.colorMultiplier(world, x, y, z);
        float f1 = (float) (l >> 16 & 255) / 255.0f;
        float f2 = (float) (l >> 8 & 255) / 255.0f;
        float f3 = (float) (l & 255) / 255.0f;
        if (EntityRenderer.anaglyphEnable) {
            float f4 = (f1 * 30.0f + f2 * 59.0f + f3 * 11.0f) / 100.0f;
            float f5 = (f1 * 30.0f + f2 * 70.0f) / 100.0f;
            float f6 = (f1 * 30.0f + f3 * 70.0f) / 100.0f;
            f1 = f4;
            f2 = f5;
            f3 = f6;
        }
        tessellator.setColorOpaque_F(f * f1, f * f2, f * f3);
        float[] sizes = ko.getSizes();
        float fx = (float) x + 0.5f;
        float fz = (float) z + 0.5f;
        fx = z % 2 == 0 ? (fx += 0.01f) : (fx -= 0.01f);
        double d3 = icon.getMinU();
        double d4 = icon.getMinV();
        double d5 = icon.getMaxU();
        double d6 = icon.getMaxV();
        this.automaticTessel(fx - sizes[0], y, fz - sizes[2], fx + sizes[0], (float) y + sizes[1], fz + sizes[2], d3, d5, d4, d6, tessellator);
        this.automaticTessel(fx + sizes[0], y, fz - sizes[2], fx - sizes[0], (float) y + sizes[1], fz + sizes[2], d3, d5, d4, d6, tessellator);
        return true;
    }

    public boolean shouldRender3DInInventory(int modelId) {
        return false;
    }

    public int getRenderId() {
        return KustProxy.kustRenderId;
    }
}

