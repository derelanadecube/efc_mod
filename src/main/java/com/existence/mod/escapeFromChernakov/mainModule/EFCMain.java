package com.existence.mod.escapeFromChernakov.mainModule;

import com.existence.mod.escapeFromChernakov.mainModule.proxy.ClientProxy;
import com.existence.mod.escapeFromChernakov.mainModule.proxy.CommonProxy;
import com.existence.mod.escapeFromChernakov.util.EFCLogger;
import com.google.common.base.Stopwatch;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.FMLEventChannel;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraftforge.common.config.Configuration;
import org.lwjgl.opengl.Display;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

//import com.existence.mod.escapeFromChernakov.util.UnicodeFontRenderer;
//import com.existence.mod.escapeFromChernakov.util.annotations.time.Time;

@Mod(modid = EFCReference.MOD_ID,
        name = EFCReference.MOD_NAME,
        version = EFCReference.VERSION)
public class EFCMain {

    @Mod.Instance(value = EFCReference.MOD_ID)
    public static EFCMain efcInstance;

    public CommonProxy commonProxy;
    public static FMLEventChannel DISPATCHER;
    public static SimpleNetworkWrapper mainNetworkChannel;
    public static Configuration configFile;
    private static File efcDir;
    public static Side side = FMLCommonHandler.instance().getEffectiveSide();
    public boolean isMainMenuLoaded = false;
    public static ArrayList<String> unlocalizedNames = new ArrayList<>();
    public static boolean printDebugLog = true;
    public static boolean printStackTrace = false;

    @Mod.EventHandler
    //@Time(format = "Method time: %s ms")
    public void preInit(FMLPreInitializationEvent event) {
        checkJavaVersion();

        Stopwatch watch = Stopwatch.createStarted();

        /**
         * Редактируем название рабочего окна и его иконку при помощи метода {@link EFCMain#readImage}
         */
        if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
            Display.setTitle("Existence - EFC mod " + EFCReference.VERSION);
            if (Util.getOSType() != Util.EnumOS.OSX) {
                try {
                    Display.setIcon(new ByteBuffer[]{
                            this.readImage(Minecraft.getMinecraft().getResourceManager().getResource(
                                    new ResourceLocation("escapefromchernakov:textures/gui/icon_16x16.png")).getInputStream()),
                            this.readImage(Minecraft.getMinecraft().getResourceManager().getResource(
                                    new ResourceLocation("escapefromchernakov:textures/gui/icon_32x32.png")).getInputStream())
                    });

                    EFCLogger.debug("Loaded Icon");
                } catch (IOException ioexception) {
                    EFCLogger.bigWarning("Error loading Icon.", ioexception.getStackTrace());
                }
            }
        }

        configFile = new Configuration(event.getSuggestedConfigurationFile());
        syncConfig();
        efcDir = new File(event.getModConfigurationDirectory().getParentFile(), "/EFC/");
        if (!efcDir.exists()) {
            EFCLogger.bigWarning("EFC folder not found. Creating empty folder.");
            EFCLogger.bigWarning("You should get some content and put them in the EFC folder.");
            efcDir.mkdirs();
            efcDir.mkdir();
        }

        /**
         * Вызов всех эвентских клиентов здесь, что бы не засорять главный клсс {@link EFCMain}
         * Эвенты клиента {@link ClientProxy#preInit(FMLPreInitializationEvent)}
         */
        if (event.getSide().isClient()) {
            ClientProxy.preInit(event);
            //ClientProxy.preInitSecondStage(event);
        }

        //commonProxy.registerGuiEvents(); //TODO: ПОФИКСИТЬ ЕБУЧЕЕ БЛЯДСКОЕ МЕНЮ СУКА С ШРИФТАМИ ЗАЕБЛО НАХУЙ БЛЯДЬ НИХУЯ НЕ РАБОТАЕТ СУКА

        EFCLogger.info("EFC mod preInit successfully loaded after: " + watch.elapsed(TimeUnit.SECONDS) + "sec, " + watch.elapsed(TimeUnit.MILLISECONDS) + "ms");
    }

    public void init(FMLInitializationEvent event) {

        if (event.getSide().isClient()) {
            /**
             * @class UnicodeFontRenderer - класс для загрузки кастомных TTF шрифтов.
             *  Нужно доработать, что бы не было артефактов.
             */
            //if (Backend.client) { //TODO: сделать интерфейс разделения клиент/сервер.
                //try {
                //    UnicodeFontRenderer.init();
                //} catch (SlickException ex) {
                //    EFCLogger.bigWarning("Failed to load fonts!", ex);
                //}
            //}
        }
    }

    @Mod.EventHandler
    @SuppressWarnings("unchecked")
    public void postInit(FMLPostInitializationEvent event) {

        Stopwatch watch = Stopwatch.createStarted();

        //commonProxy.postInit();
        EFCLogger.debug("==================================================");
        for (String name : unlocalizedNames) {
            EFCLogger.info("needs translation: " + name);
        }
        EFCLogger.debug("==================================================");

        EFCLogger.info("EFC mod postInit successfully loaded after: " + watch.elapsed(TimeUnit.SECONDS) + "sec, " + watch.elapsed(TimeUnit.MILLISECONDS) + "ms");
    }

    public static void syncConfig() {
        printDebugLog = configFile.getBoolean("Print Debug Log", "general", printDebugLog, "");
        printStackTrace = configFile.getBoolean("Print Stack Trace", "general", printStackTrace, "");

        if (configFile.hasChanged()) {
            configFile.save();
        }
    }

    private ByteBuffer readImage(InputStream par1File) throws IOException {
        BufferedImage bufferedimage = ImageIO.read(par1File);
        int[] aint = bufferedimage.getRGB(0, 0, bufferedimage.getWidth(), bufferedimage.getHeight(), null, 0, bufferedimage.getWidth());
        ByteBuffer bytebuffer = ByteBuffer.allocate(4 * aint.length);
        int[] aint1 = aint;
        int i = aint.length;
        for (int j = 0; j < i; ++j) {
            int k = aint1[j];
            bytebuffer.putInt(k << 8 | k >> 24 & 255);
        }
        bytebuffer.flip();
        return bytebuffer;
    }

    private void checkJavaVersion() {
        String versionString = System.getProperty("java.version");
        int pos = versionString.indexOf('.');
        pos = versionString.indexOf('.', pos + 1);
        double version = Double.parseDouble(versionString.substring(0, pos));
        if (version < 1.8) {
            EFCLogger.warn("EFC only supports Java 8 and above. Please update your Java version. You are currently using: " + version);
        }
    }
}