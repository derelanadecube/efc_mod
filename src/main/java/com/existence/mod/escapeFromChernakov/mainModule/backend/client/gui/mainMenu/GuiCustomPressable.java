package com.existence.mod.escapeFromChernakov.mainModule.backend.client.gui.mainMenu;

import com.existence.mod.escapeFromChernakov.util.EFCUtils;
import com.existence.mod.escapeFromChernakov.util.UnicodeFontRenderer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 17.06.2018
 * Time: 12:45
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */

@SideOnly(Side.CLIENT)
public class GuiCustomPressable extends GuiButton {

    public int isOver = 2;
    private ResourceLocation iconTexture = null;
    public boolean drawBackground = true;
    public boolean drawShadow = true;
    public Color menuTheme = new Color(47, 47, 47);
    public boolean centeredText = true;
    private int fade = 0;
    public boolean soundPlayed = true;

    public GuiCustomPressable(int id, int x, int y, int width, int height, String displayString) {
        super(id, x, y, width, height, displayString);
    }

    public GuiCustomPressable(int id, int x, int y, int width, int height, String displayString, Color menuTheme) {
        super(id, x, y, width, height, displayString);
        this.menuTheme = menuTheme;
    }

    public GuiCustomPressable(int id, int x, int y, int width, int height, ResourceLocation iconTexture) {
        super(id, x, y, width, height, "");
        this.iconTexture = iconTexture;
    }

    public void drawButton(Minecraft minecraft, int mouseX, int mouseY) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        UnicodeFontRenderer fontRenderer2;
        if (this.visible) {
            GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
            this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
            this.isOver = this.getHoverState(this.field_146123_n);
            if (this.drawBackground) {
                if (this.drawShadow) {
                    EFCUtils.drawRect(this.xPosition - 1, this.yPosition - 1, this.width + 2, this.height + 2, new Color(0, 0, 0, 55));
                    EFCUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height, new Color(0, 0, 0, 90));
                } else {
                    EFCUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height, this.menuTheme);
                }
            }
            this.mouseDragged(minecraft, mouseX, mouseY);
            String displayText = this.displayString;
            if (this.isOver == 2) {
                this.fade = this.fade <= 0 ? 0 : (this.fade -= 15);
                EFCUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height, new Color(40, 40, 40, this.fade));
                if (!this.soundPlayed) {
                    Minecraft.getMinecraft().getSoundHandler().playSound((ISound) PositionedSoundRecord.func_147674_a((ResourceLocation) new ResourceLocation("escapefromchernakov:gui.menu.keyhover"), (float) 2.5f));
                    this.soundPlayed = true;
                }
            } else {
                this.fade = 90;
                this.soundPlayed = false;
            }
            if (this.iconTexture != null) {
                if (this.isOver == 2) {
                    Color color = new Color(128, 128, 128);
                    GL11.glColor3f((float) ((float) color.getRed() / 255.0f), (float) ((float) color.getGreen() / 255.0f), (float) ((float) color.getBlue() / 255.0f));
                }
                EFCUtils.drawImage(this.xPosition + this.width / 2 - 8, this.yPosition + (this.height - 8) - 14, this.iconTexture, 16.0, 16.0);
                GL11.glColor3f((float) 1.0f, (float) 1.0f, (float) 1.0f);
                return;
            }
            if (!this.enabled) {
                displayText = displayText;
            }
            if (!this.centeredText) {
                UnicodeFontRenderer.renderString(displayText, this.xPosition + 2, this.yPosition + (this.height - 8) / 2);
                //fontRenderer.drawStringWithShadow(displayText, this.xPosition + 2, this.yPosition + (this.height - 8) / 2, this.isOver == 2 ? 8421504 : 16777215);
                return;
            }
            UnicodeFontRenderer.renderString(displayText, this.xPosition + this.width / 2 - fontRenderer.getStringWidth(displayText) / 2, this.yPosition + (this.height - 8) / 2);
            //fontRenderer.drawStringWithShadow(displayText, this.xPosition + this.width / 2 - fontRenderer.getStringWidth(displayText) / 2, this.yPosition + (this.height - 8) / 2, this.isOver == 2 ? 8421504 : 16777215);
        }
    }

    public void func_146113_a(SoundHandler soundHandler) {
        soundHandler.playSound((ISound) PositionedSoundRecord.func_147674_a((ResourceLocation) new ResourceLocation("escapefromchernakov:gui.menu.keypress"), (float) 1.0f));
    }
}