package com.existence.mod.escapeFromChernakov.mainModule.proxy;

import com.existence.mod.escapeFromChernakov.mainModule.backend.client.events.ClientGuiEvents;
import com.existence.mod.escapeFromChernakov.mainModule.backend.client.events.ClientMenuEvents;
import com.existence.mod.escapeFromChernakov.util.EFCLogger;
import com.existence.mod.escapeFromChernakov.util.TextureMap;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;

import java.io.File;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 15.05.2018
 * Time: 11:46
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class ClientProxy extends CommonProxy {

    private static String resourceFolder;
    public static ClientGuiEvents clientGuiEvents;

    @SideOnly(Side.CLIENT)
    public static void preInit(FMLPreInitializationEvent event) {
        Minecraft mc = Minecraft.getMinecraft();
        clientGuiEvents = new ClientGuiEvents(mc);
        FMLCommonHandler.instance().bus().register(clientGuiEvents);
        MinecraftForge.EVENT_BUS.register(clientGuiEvents);
        String s = event.getModConfigurationDirectory().getAbsolutePath();
        resourceFolder = s.substring(0, s.length() - 7) + File.separator + "res";
        TextureMap.load(resourceFolder + File.separator + "textures");
        EFCLogger.debug("Loaded texture list");
    }

    @SideOnly(Side.CLIENT)
    public static void registerGuiEvents() {
        MinecraftForge.EVENT_BUS.register(new ClientMenuEvents());
    }

    public static void init(FMLInitializationEvent event) {
        //FMLCommonHandler.instance().bus().register(new ClientMainEvents());
        EFCLogger.debug("Initializated Client Events");
    }
}
