package com.existence.mod.escapeFromChernakov;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 17.06.2018
 * Time: 13:18
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface Backend {
    boolean client = true;
    boolean server = true;
}
