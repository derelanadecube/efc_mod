package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown;

import com.existence.mod.escapeFromChernakov.util.EFCLogger;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.MathHelper;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 12:48
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class WatchdogTask extends TimerTask {

    private static WatchdogTask INSTANCE;
    private static MinecraftServer SERVER;
    private int lastTick = 0;
    private int hungTicks = 0;
    private int lagTicks = 0;
    private boolean isHanging = false;

    public static void create() {
        if (INSTANCE != null) {
            throw new RuntimeException("WatchdogTask can only be created once");
        } else {
            INSTANCE = new WatchdogTask();
            SERVER = MinecraftServer.getServer();
            Timer timer = new Timer("AutoShutdown watchdog");
            int intervalMs = Config.watchdogInterval * 1000;
            timer.schedule(INSTANCE, (long) intervalMs, (long) intervalMs);
            EFCLogger.debug("Watchdog timer running");
        }
    }

    public void run() {
        if (this.isHanging) {
            this.doHanging();
        } else {
            this.doMonitor();
        }

    }

    private void doMonitor() {
        double latency = MathHelper.average(SERVER.tickTimeArray) * 1.0E-6D;
        double tps = Math.min(1000.0D / latency, 20.0D);
        EFCLogger.info("Watchdog: 100 tick avg. latency: %.2f / 50 ms", new Object[]{Double.valueOf(latency)});
        EFCLogger.info("Watchdog: 100 tick avg. TPS: %.2f / 20", new Object[]{Double.valueOf(tps)});

        int serverTick = SERVER.getTickCounter();
        if (serverTick == this.lastTick) {
            EFCLogger.debug("No advance in server ticks; server is hanging");
            this.isHanging = true;
            this.hungTicks = 1;
        } else {
            this.lastTick = serverTick;
            if (tps < (double) Config.lowTPSThreshold) {
                ++this.lagTicks;
                int lagSec = this.lagTicks * Config.watchdogInterval;
                EFCLogger.info("TPS too low since %d seconds", new Object[]{Integer.valueOf(lagSec)});
                if (lagSec >= Config.lowTPSTimeout) {
                    EFCLogger.warn("TPS below %d since %d seconds", new Object[]{Integer.valueOf(Config.lowTPSThreshold), Integer.valueOf(lagSec)});
                    if (Config.attemptSoftKill) {
                        this.performSoftKill();
                    } else {
                        this.performHardKill();
                    }
                }
            } else {
                this.lagTicks = 0;
            }

        }
    }

    private void doHanging() {
        int serverTick = SERVER.getTickCounter();
        if (serverTick != this.lastTick) {
            EFCLogger.debug("Server no longer hanging");
            this.isHanging = false;
        } else {
            ++this.hungTicks;
            int hangSec = this.hungTicks * Config.watchdogInterval;
            EFCLogger.info("Server hanging for %d seconds", new Object[]{Integer.valueOf(hangSec)});
            if (hangSec >= Config.maxTickTimeout) {
                EFCLogger.warn("Server is hung on a tick after %d seconds", new Object[]{Integer.valueOf(hangSec)});
                if (Config.attemptSoftKill) {
                    this.performSoftKill();
                } else {
                    this.performHardKill();
                }
            }

        }
    }

    private void performSoftKill() {
        EFCLogger.warn("Attempting a soft kill of the server...");
        Thread hardKillCheck = new Thread("Shutdown watchdog") {
            public void run() {
                try {
                    Thread.sleep(10000L);
                    System.out.println("Hung during soft kill; trying a hard kill..");
                    WatchdogTask.this.performHardKill();
                } catch (InterruptedException var2) {
                    ;
                }

            }
        };
        hardKillCheck.start();
        FMLCommonHandler.instance().exitJava(1, false);
    }

    private void performHardKill() {
        EFCLogger.warn("Attempting a hard kill of the server - data may be lost!");
        FMLCommonHandler.instance().exitJava(1, true);
    }
}
