package com.existence.mod.escapeFromChernakov.radialMenuModule.data.menu;

import com.existence.mod.escapeFromChernakov.radialMenuModule.data.click.ClickAction;
import com.google.gson.annotations.SerializedName;
import net.minecraft.item.ItemStack;

/**
 * @author tp3rson
 */
public class MenuItem {

    public final String title;

    public final ItemStack icon;

    @SerializedName("action")
    public final ClickAction.IClickAction clickAction;

    public MenuItem(String title, ItemStack icon, ClickAction.IClickAction clickAction) {
        this.title = title;
        this.icon = icon;
        this.clickAction = clickAction;
    }

    public void onRemoved() {
        if (clickAction != null) {
            clickAction.onRemoved();
        }
    }
}
