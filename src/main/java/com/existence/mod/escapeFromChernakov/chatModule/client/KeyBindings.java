package com.existence.mod.escapeFromChernakov.chatModule.client;

import cpw.mods.fml.client.registry.ClientRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import org.apache.commons.lang3.ArrayUtils;

public class KeyBindings {

    public static KeyBinding chatOpen;
    public static KeyBinding chatCommand;
    public static KeyBinding chatReply;


    public static void init() {
        Minecraft mc = Minecraft.getMinecraft();
        KeyBinding mcChatOpen = mc.gameSettings.keyBindChat;
        KeyBinding mcChatCommand = mc.gameSettings.keyBindCommand;
        if (mcChatOpen.getKeyCode() != 0) {
            chatOpen = new KeyBinding("talkative.chatopen", mcChatOpen.getKeyCode(), mcChatOpen.getKeyCategory());
        } else {
            chatOpen = new KeyBinding("talkative.chatopen", mcChatOpen.getKeyCodeDefault(), mcChatOpen.getKeyCategory());
        }

        if (mcChatCommand.getKeyCode() != 0) {
            chatCommand = new KeyBinding("talkative.chatcmd", mcChatCommand.getKeyCode(), mcChatOpen.getKeyCategory());
        } else {
            chatCommand = new KeyBinding("talkative.chatcmd", mcChatCommand.getKeyCodeDefault(), mcChatOpen.getKeyCategory());
        }

        chatReply = new KeyBinding("talkative.chatreply", 19, mcChatOpen.getKeyCategory());
        mcChatOpen.setKeyCode(0);
        mcChatCommand.setKeyCode(0);
        ClientRegistry.registerKeyBinding(chatOpen);
        ClientRegistry.registerKeyBinding(chatCommand);
        ClientRegistry.registerKeyBinding(chatReply);
        Minecraft.getMinecraft().gameSettings.keyBindings = (KeyBinding[]) ArrayUtils.removeElement(Minecraft.getMinecraft().gameSettings.keyBindings, mcChatOpen);
        Minecraft.getMinecraft().gameSettings.keyBindings = (KeyBinding[]) ArrayUtils.removeElement(Minecraft.getMinecraft().gameSettings.keyBindings, mcChatCommand);
    }
}
