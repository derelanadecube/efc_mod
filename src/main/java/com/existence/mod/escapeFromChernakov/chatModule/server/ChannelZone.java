package com.existence.mod.escapeFromChernakov.chatModule.server;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChannelEvent;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChatWrapperServer;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

import java.util.Iterator;

public class ChannelZone extends Channel {

    private double x;
    private double z;
    public double xmin;
    public double xmax;
    public double zmin;
    public double zmax;


    public ChannelZone(String name) {
        super(name);
    }

    public ChannelZone(String name, double x, double z) {
        super(name);
        this.x = x;
        this.z = z;
    }

    public void addSecondPoint(double x2, double z2) {
        this.xmin = Math.min(this.x, x2);
        this.xmax = Math.max(this.x, x2);
        this.zmin = Math.min(this.z, z2);
        this.zmax = Math.max(this.z, z2);
    }

    public ReturnStatus checkPlayer(EntityPlayerMP player) {
        return this.isPlayerInside(player) && !super.users.contains(player) ? this.addPlayer(player) : (!this.isPlayerInside(player) && super.users.contains(player) ? this.rmPlayer(player, true) : new ReturnStatus(ReturnStatus.Status.CRITICAL, "Dafuk ? How did you managed to get inside this channel ???"));
    }

    public boolean isPlayerInside(EntityPlayerMP player) {
        return player.posX <= this.xmax && player.posX >= this.xmin && player.posZ <= this.zmax && player.posZ >= this.zmin;
    }

    public ReturnStatus addPlayer(EntityPlayerMP player) {
        Iterator i$ = super.users.iterator();

        while (i$.hasNext()) {
            EntityPlayerMP p = (EntityPlayerMP) i$.next();
            NetworkHelper.sendTo(new MsgChatWrapperServer(String.format("* %s joined the channel *", new Object[]{player.getDisplayName()}), this.getName(), this.getName()), p);
        }

        NetworkHelper.sendTo(new MsgChatWrapperServer(String.format("* You have joined the zone %s *", new Object[]{this.getName()}), Talkative.globalChat, this.getName()), player);
        super.users.add(player);
        this.dispatchUserList();
        NetworkHelper.sendTo(new MsgChannelEvent(super.name, player.getDisplayName(), ChannelEvent.JOIN), player);
        return new ReturnStatus(ReturnStatus.Status.OK, String.format("Joined channel %s.", new Object[]{this.getName()}));
    }

    public ReturnStatus rmPlayer(EntityPlayerMP player, boolean forced) {
        super.users.remove(player);
        this.dispatchUserList();
        Iterator i$ = super.users.iterator();

        while (i$.hasNext()) {
            EntityPlayerMP p = (EntityPlayerMP) i$.next();
            NetworkHelper.sendTo(new MsgChatWrapperServer(String.format("* %s left the channel *", new Object[]{player.getDisplayName()}), this.getName(), this.getName()), p);
        }

        NetworkHelper.sendTo(new MsgChatWrapperServer(String.format("* You have left the zone %s *", new Object[]{this.getName()}), Talkative.globalChat, this.getName()), player);
        NetworkHelper.sendTo(new MsgChannelEvent(super.name, player.getDisplayName(), ChannelEvent.PART), player);
        return new ReturnStatus(ReturnStatus.Status.OK, String.format("Left channel %s.", new Object[]{this.getName()}));
    }

    public ReturnStatus invitePlayer(ICommandSender sender, EntityPlayerMP target) {
        return new ReturnStatus(ReturnStatus.Status.ERROR, "You cannot invite a player to a zone chat");
    }

    public ReturnStatus revokePlayer(ICommandSender sender, EntityPlayerMP target) {
        return new ReturnStatus(ReturnStatus.Status.ERROR, "You cannot invite or revoke an invitation to a zone chat");
    }

    public ChannelZone readFromNBT(NBTTagCompound tag) {
        super.readFromNBT(tag);
        NBTTagCompound coordTag = tag.getCompoundTag("coords");
        this.xmin = coordTag.getDouble("xmin");
        this.xmax = coordTag.getDouble("xmax");
        this.zmin = coordTag.getDouble("zmin");
        this.zmax = coordTag.getDouble("zmax");
        return this;
    }

    public void writeToNBT(NBTTagCompound tag) {
        super.writeToNBT(tag);
        NBTTagCompound coordTag = new NBTTagCompound();
        coordTag.setDouble("xmin", this.xmin);
        coordTag.setDouble("xmax", this.xmax);
        coordTag.setDouble("zmin", this.zmin);
        coordTag.setDouble("zmax", this.zmax);
        tag.setTag("coords", coordTag);
    }

    public Channel setDefaultAccess() {
        this.set(ChanStat.JOIN, false).set(ChanStat.PART, false).set(ChanStat.MODE, false).set(ChanStat.GLOBAL, true).set(ChanStat.PERMANENT, true);
        return this;
    }
}
