package com.existence.mod.escapeFromChernakov.chatModule.client.gui;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChannelSwitch;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.utils.StringHelper;
import com.google.common.collect.Lists;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MathHelper;
import org.lwjgl.opengl.GL11;

import java.text.SimpleDateFormat;
import java.util.*;

public class GuiChatText extends GuiNewChat {

    private Minecraft mc;
    private String channelRecv;
    private String channelDisp;
    private String channelLast;
    private Map buffers;
    private Map unread;
    private Map received;
    private List unreadNotices;
    private Map users;
    private Map listw;
    private boolean dirty;


    public GuiChatText(Minecraft mc) {
        super(mc);
        this.channelRecv = Talkative.globalChat;
        this.channelDisp = Talkative.globalChat;
        this.channelLast = Talkative.globalChat;
        this.buffers = new LinkedHashMap();
        this.unread = new LinkedHashMap();
        this.received = new LinkedHashMap();
        this.unreadNotices = new ArrayList();
        this.users = new LinkedHashMap();
        this.listw = new LinkedHashMap();
        this.dirty = false;
        this.mc = mc;
        this.switchReceivingChannel(Talkative.globalChat);
        this.switchDisplayChannel(Talkative.globalChat);
    }

    public void switchReceivingChannel(String channel) {
        this.softAddChannel(channel);
        this.channelRecv = channel;
    }

    public void switchDisplayChannel(String channel) {
        this.softAddChannel(channel);
        this.channelDisp = channel;
        NetworkHelper.sendToServer(new MsgChannelSwitch(channel));
    }

    public void setUserList(String channel, Set users) {
        this.users.put(channel, users);
        int maxw = 0;

        String s;
        for (Iterator i$ = users.iterator(); i$.hasNext(); maxw = Math.max(maxw, this.mc.fontRenderer.getStringWidth(s))) {
            s = (String) i$.next();
        }

        this.listw.put(channel, Integer.valueOf(maxw));
    }

    public Set getUserList() {
        return (Set) this.users.get(this.channelDisp);
    }

    public int getUserWidth() {
        return ((Integer) this.listw.get(this.channelDisp)).intValue();
    }

    public boolean isDirty() {
        return this.dirty;
    }

    public void setDirty(boolean flag) {
        this.dirty = flag;
    }

    public void softAddChannel(String channel) {
        if (channel.equals(Talkative.globalChat) && !this.buffers.containsKey(channel)) {
            this.buffers.put(channel, new GuiNewChat(this.mc));
            this.unread.put(channel, Integer.valueOf(0));
            this.received.put(channel, Integer.valueOf(0));
            String backRecv = this.channelRecv;
            this.switchReceivingChannel(channel);
            GuiNewChat oldChat = (GuiNewChat) Talkative.proxy.getOldGlobalChan();
            Iterator i$ = oldChat.chatLines.iterator();

            while (i$.hasNext()) {
                Object o = i$.next();
                ChatLine l = (ChatLine) o;
                this.printChatMessage(l.func_151461_a(), false);
            }

            this.switchReceivingChannel(backRecv);
            this.dirty = true;
        }

        if (!this.buffers.containsKey(channel)) {
            this.buffers.put(channel, new GuiNewChat(this.mc));
            this.unread.put(channel, Integer.valueOf(0));
            this.received.put(channel, Integer.valueOf(0));
            this.dirty = true;
        }

    }

    public void rmChannel(String channel) {
        if (this.buffers.containsKey(channel)) {
            this.buffers.remove(channel);
            this.unread.remove(channel);
            this.dirty = true;
        }

    }

    public Set getChannels() {
        return this.buffers.keySet();
    }

    public String getChannelDisplay() {
        return this.channelDisp;
    }

    public String getChannelRecv() {
        return this.channelRecv;
    }

    public String getChannelLast() {
        return this.channelLast;
    }

    public void resetUnread(String channel) {
        this.unread.put(channel, Integer.valueOf(0));
        this.setButtons();
    }

    public int getUnread(String channel) {
        return this.unread.containsKey(channel) ? ((Integer) this.unread.get(channel)).intValue() : 0;
    }

    public int getReceived() {
        return this.getReceived(this.channelDisp);
    }

    public int getReceived(String channel) {
        return this.received.containsKey(channel) ? ((Integer) this.received.get(channel)).intValue() : 0;
    }

    private void setButtons() {
        this.unreadNotices.clear();
        Iterator i$ = this.getChannels().iterator();

        while (i$.hasNext()) {
            String channel = (String) i$.next();
            int unread = this.getUnread(channel);
            if (unread > 0) {
                int strWidth = this.mc.fontRenderer.getStringWidth(channel) + 16;
                strWidth += this.mc.fontRenderer.getStringWidth(" [" + unread + "]");
                int xpos = 0;

                GuiTab tab1;
                for (Iterator tab = this.unreadNotices.iterator(); tab.hasNext(); xpos += tab1.width) {
                    tab1 = (GuiTab) tab.next();
                }

                GuiTab tab2 = new GuiTab(this.unreadNotices.size() + 1, xpos, 52, strWidth, 12, channel);
                this.unreadNotices.add(tab2);
            }
        }

    }

    public void drawChat(int tick) {
        ((GuiNewChat) this.buffers.get(this.channelDisp)).drawChat(tick);
        if (this.mc.currentScreen == null && Talkative.showNotices) {
            GL11.glPushMatrix();
            GL11.glScalef(0.75F, 0.75F, 0.75F);
            Iterator i$ = this.unreadNotices.iterator();

            while (i$.hasNext()) {
                GuiTab tab = (GuiTab) i$.next();
                tab.drawButton(this.mc, 0, 0);
            }

            GL11.glPopMatrix();
        }

    }

    public void printChatMessage(IChatComponent msg) {
        this.printChatMessage(msg, true);
    }

    public void printChatMessage(IChatComponent msg, boolean timestamp) {
        Date date = new Date();
        new ChatComponentText(String.format("[%s]", new Object[]{this.channelRecv}));
        ChatComponentText stampComponent = new ChatComponentText("");
        SimpleDateFormat sdfDate;
        if (timestamp && Talkative.showTimestamp && Talkative.timestamp24h) {
            sdfDate = new SimpleDateFormat("HH:mm:ss");
            stampComponent = new ChatComponentText(String.format("[%s] ", new Object[]{sdfDate.format(date)}));
        }

        if (timestamp && Talkative.showTimestamp && !Talkative.timestamp24h) {
            sdfDate = new SimpleDateFormat("KK:mm:ss a");
            stampComponent = new ChatComponentText(String.format("[%s] ", new Object[]{sdfDate.format(date)}));
        }

        stampComponent.appendSibling(msg);
        if (!this.channelRecv.equals(this.channelDisp)) {
            this.unread.put(this.channelRecv, Integer.valueOf(((Integer) this.unread.get(this.channelRecv)).intValue() + 1));
            this.setDirty(true);
            this.setButtons();
        }

        this.received.put(this.channelRecv, Integer.valueOf(((Integer) this.received.get(this.channelRecv)).intValue() + this.computeLines(stampComponent)));
        ((GuiNewChat) this.buffers.get(this.channelRecv)).printChatMessage(stampComponent);
        this.channelLast = this.channelRecv;
    }

    public void scroll(int i) {
        ((GuiNewChat) this.buffers.get(this.channelDisp)).scroll(i);
    }

    private int computeLines(IChatComponent msg) {
        int chatWidth = MathHelper.floor_float((float) this.func_146228_f() / this.func_146244_h());
        int l = 0;
        ChatComponentText targetComponent = new ChatComponentText("");
        ArrayList trgArray = Lists.newArrayList();
        ArrayList msgArray = Lists.newArrayList(msg);

        for (int i1 = 0; i1 < msgArray.size(); ++i1) {
            IChatComponent subComponent0 = (IChatComponent) msgArray.get(i1);
            String stmp = subComponent0.getChatStyle().getFormattingCode() + subComponent0.getUnformattedTextForChat();
            String s = this.mc.gameSettings.chatColours ? stmp : StringHelper.getTextWithoutFormattingCodes(stmp);
            int msgWidth = this.mc.fontRenderer.getStringWidth(s);
            ChatComponentText subComponent1 = new ChatComponentText(s);
            subComponent1.setChatStyle(subComponent0.getChatStyle().createShallowCopy());
            boolean flag = false;
            if (l + msgWidth > chatWidth) {
                String s1 = this.mc.fontRenderer.trimStringToWidth(s, chatWidth - l, false);
                String s2 = s1.length() < s.length() ? s.substring(s1.length()) : null;
                if (s2 != null && s2.length() > 0) {
                    int k1 = s1.lastIndexOf(" ");
                    if (k1 >= 0 && this.mc.fontRenderer.getStringWidth(s.substring(0, k1)) > 0) {
                        s1 = s.substring(0, k1);
                        s2 = s.substring(k1);
                    }

                    ChatComponentText chatcomponenttext2 = new ChatComponentText(s2);
                    chatcomponenttext2.setChatStyle(subComponent0.getChatStyle().createShallowCopy());
                    msgArray.add(i1 + 1, chatcomponenttext2);
                }

                msgWidth = this.mc.fontRenderer.getStringWidth(s1);
                subComponent1 = new ChatComponentText(s1);
                subComponent1.setChatStyle(subComponent0.getChatStyle().createShallowCopy());
                flag = true;
            }

            if (l + msgWidth <= chatWidth) {
                l += msgWidth;
                targetComponent.appendSibling(subComponent1);
            } else {
                flag = true;
            }

            if (flag) {
                trgArray.add(targetComponent);
                l = 0;
                targetComponent = new ChatComponentText("");
            }
        }

        trgArray.add(targetComponent);
        return trgArray.size();
    }

    public IChatComponent func_146236_a(int x, int y) {
        IChatComponent msg = ((GuiNewChat) this.buffers.get(this.channelDisp)).func_146236_a(x, y);
        return msg;
    }

    public ChatLine getChatLineAtCoordinates(int x, int y) {
        GuiNewChat currDisplay = (GuiNewChat) this.buffers.get(this.channelDisp);
        if (!currDisplay.getChatOpen()) {
            return null;
        } else {
            ScaledResolution scaledresolution = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
            int k = scaledresolution.getScaleFactor();
            float f = this.func_146244_h();
            int l = x / k - 3;
            int i1 = y / k - 27;
            l = MathHelper.floor_float((float) l / f);
            i1 = MathHelper.floor_float((float) i1 / f);
            if (l >= 0 && i1 >= 0) {
                int j1 = Math.min(this.func_146232_i(), currDisplay.field_146253_i.size());
                if (l <= MathHelper.floor_float((float) this.func_146228_f() / this.func_146244_h()) && i1 < this.mc.fontRenderer.FONT_HEIGHT * j1 + j1) {
                    int k1 = i1 / this.mc.fontRenderer.FONT_HEIGHT + currDisplay.field_146250_j;
                    if (k1 >= 0 && k1 < currDisplay.field_146253_i.size()) {
                        ChatLine chatline = (ChatLine) currDisplay.field_146253_i.get(k1);
                        return chatline;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }
}
