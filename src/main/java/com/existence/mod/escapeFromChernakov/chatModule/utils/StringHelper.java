package com.existence.mod.escapeFromChernakov.chatModule.utils;

import java.util.regex.Pattern;

public class StringHelper {

    private static final Pattern formattingCodePattern = Pattern.compile("(?i)" + String.valueOf('\u00a7') + "[0-9A-FK-OR]");


    public static String getTextWithoutFormattingCodes(String msg) {
        return msg == null ? null : formattingCodePattern.matcher(msg).replaceAll("");
    }

}
