package com.existence.mod.escapeFromChernakov.chatModule.commands;

import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import com.existence.mod.escapeFromChernakov.chatModule.server.ReturnStatus;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;

import java.util.Iterator;
import java.util.Set;

public class CommandNames extends CommandBase {

    public String getCommandName() {
        return "names";
    }

    public String getCommandUsage(ICommandSender sender) {
        return "/names <channel>. Channels start with a #";
    }

    public void processCommand(ICommandSender sender, String[] data) {
        if (data.length >= 1 && data[0].startsWith("#")) {
            if (ChannelHandler.INSTANCE.getChannel(data[0]) == null) {
                sender.addChatMessage(new ChatComponentText(String.format("No such channel : %s", new Object[]{data[0]})));
            } else {
                Set players = ChannelHandler.INSTANCE.getChannel(data[0]).getUsers();
                Iterator i$ = players.iterator();

                while (i$.hasNext()) {
                    EntityPlayerMP player = (EntityPlayerMP) i$.next();
                    sender.addChatMessage(new ChatComponentText(String.format("+ %s", new Object[]{player.getDisplayName()})));
                }
            }

        } else {
            NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, this.getCommandUsage(sender)), sender);
        }
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
