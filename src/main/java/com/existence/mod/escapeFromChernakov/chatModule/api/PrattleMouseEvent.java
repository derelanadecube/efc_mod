package com.existence.mod.escapeFromChernakov.chatModule.api;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.util.IChatComponent;

public class PrattleMouseEvent extends Event {


    public static class ChatHover extends PrattleMouseEvent {

        public final IChatComponent hoverText;
        public final IChatComponent fullText;
        public final int xpos;
        public final int ypos;


        public ChatHover(IChatComponent hoverText, IChatComponent fullText, int xpos, int ypos) {
            this.hoverText = hoverText;
            this.fullText = fullText;
            this.xpos = xpos;
            this.ypos = ypos;
        }
    }

    @Cancelable
    public static class ChatClick extends PrattleMouseEvent {

        public final IChatComponent clickedText;
        public final IChatComponent fullText;
        public final int button;


        public ChatClick(IChatComponent clickedText, IChatComponent fullText, int button) {
            this.clickedText = clickedText;
            this.fullText = fullText;
            this.button = button;
        }
    }
}
