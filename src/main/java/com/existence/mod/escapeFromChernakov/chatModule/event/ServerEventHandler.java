package com.existence.mod.escapeFromChernakov.chatModule.event;

import com.existence.mod.escapeFromChernakov.chatModule.network.MsgPostlogInit;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.server.Channel;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelZone;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.event.entity.player.PlayerEvent.NameFormat;
import net.minecraftforge.event.world.WorldEvent.Load;
import net.minecraftforge.event.world.WorldEvent.Save;
import net.minecraftforge.event.world.WorldEvent.Unload;

import java.util.Iterator;

public class ServerEventHandler {

    private static long tick = 0L;


    @SubscribeEvent
    public void onNameChange(NameFormat event) {
        Iterator i$ = ChannelHandler.INSTANCE.getChannels().values().iterator();

        while (i$.hasNext()) {
            Channel chan = (Channel) i$.next();
            chan.dispatchUserList();
        }

    }

    @SubscribeEvent
    public void playerLogIn(PlayerLoggedInEvent event) {
        if (event.player instanceof EntityPlayerMP) {
            NetworkHelper.sendTo(new MsgPostlogInit(), (EntityPlayerMP) event.player);
            Iterator i$ = ChannelHandler.INSTANCE.getChannels().values().iterator();

            while (i$.hasNext()) {
                Channel chan = (Channel) i$.next();
                if (chan.get(Channel.ChanStat.FORCED)) {
                    ChannelHandler.INSTANCE.joinChannel(chan.getName(), (EntityPlayerMP) event.player);
                }
            }
        }

    }

    @SubscribeEvent
    public void playerLogOut(PlayerLoggedOutEvent event) {
        if (event.player instanceof EntityPlayerMP) {
            ChannelHandler.INSTANCE.playerDisconnected((EntityPlayerMP) event.player);
        }

    }

    @SubscribeEvent
    public void onWorldLoad(Load event) {
        if (!event.world.isRemote) {
            ChannelHandler.ChannelSaveHandler data = (ChannelHandler.ChannelSaveHandler) event.world.perWorldStorage.loadData(ChannelHandler.ChannelSaveHandler.class, ChannelHandler.ChannelSaveHandler.saveKey);
            if (data != null) {
                ChannelHandler.INSTANCE.channelSave = data;
            }

        }
    }

    @SubscribeEvent
    public void onWorldSave(Save event) {
        if (event.world.provider.dimensionId == 0) {
            if (!event.world.isRemote) {
                event.world.perWorldStorage.setData(ChannelHandler.ChannelSaveHandler.saveKey, ChannelHandler.INSTANCE.channelSave);
            }
        }
    }

    @SubscribeEvent
    public void onWorldUnload(Unload event) {
        if (event.world.provider.dimensionId == 0) {
            if (!event.world.isRemote) {
                event.world.perWorldStorage.setData(ChannelHandler.ChannelSaveHandler.saveKey, ChannelHandler.INSTANCE.channelSave);
            }
        }
    }

    @SubscribeEvent
    public void serverTick(ServerTickEvent event) {
        ++tick;
        if (tick % 20L == 0L) {
            Iterator i$ = ChannelHandler.INSTANCE.getZones().values().iterator();

            while (i$.hasNext()) {
                ChannelZone zone = (ChannelZone) i$.next();
                Iterator i$1 = MinecraftServer.getServer().getConfigurationManager().playerEntityList.iterator();

                while (i$1.hasNext()) {
                    Object o = i$1.next();
                    zone.checkPlayer((EntityPlayerMP) o);
                }
            }
        }

    }

}
