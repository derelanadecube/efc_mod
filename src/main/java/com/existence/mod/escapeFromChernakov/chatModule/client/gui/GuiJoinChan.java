package com.existence.mod.escapeFromChernakov.chatModule.client.gui;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import org.lwjgl.input.Keyboard;

public class GuiJoinChan extends GuiScreen {

    private GuiTextField inputField;


    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        this.inputField = new GuiTextField(super.fontRendererObj, 4, super.height - 12, super.width - 4, 12);
        this.inputField.setMaxStringLength(20);
        this.inputField.setEnableBackgroundDrawing(false);
        this.inputField.setFocused(true);
        this.inputField.setCanLoseFocus(false);
    }

    public void updateScreen() {
        this.inputField.updateCursorCounter();
    }

    public void drawScreen(int x, int y, float z) {
        byte h = 100;
        short w = 200;
        drawRect((super.width - w) / 2, (super.height - h) / 2, (super.width - w) / 2 + w, (super.height - h) / 2 + h, -805306368);
        this.inputField.drawTextBox();
        super.drawScreen(x, y, z);
    }
}
