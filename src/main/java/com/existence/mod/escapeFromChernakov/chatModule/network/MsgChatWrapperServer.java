package com.existence.mod.escapeFromChernakov.chatModule.network;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.api.PrattleChatEvent;
import com.existence.mod.escapeFromChernakov.chatModule.client.gui.GuiChatText;
import com.existence.mod.escapeFromChernakov.chatModule.utils.StringHelper;
import cpw.mods.fml.common.eventhandler.Event;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.server.S02PacketChat;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.IChatComponent.Serializer;
import net.minecraftforge.common.MinecraftForge;

public class MsgChatWrapperServer implements IMessage {

    private IChatComponent msg;
    private String channel;
    private String sender;


    public MsgChatWrapperServer() {
    }

    public MsgChatWrapperServer(String msg, String channel, String sender) {
        this((IChatComponent) (new ChatComponentText(msg)), channel, sender);
    }

    public MsgChatWrapperServer(IChatComponent msg, String channel, String sender) {
        this.msg = msg;
        this.channel = StringHelper.getTextWithoutFormattingCodes(channel);
        this.sender = sender;
    }

    public IChatComponent getMsg() {
        return this.msg;
    }

    public void setMsg(IChatComponent msg) {
        this.msg = msg;
    }

    public String getChannel() {
        return this.channel;
    }

    public String getSender() {
        return this.sender;
    }

    public void fromBytes(ByteBuf buf) {
        this.msg = Serializer.func_150699_a(ByteBufUtils.readUTF8String(buf));
        this.channel = ByteBufUtils.readUTF8String(buf);
        this.sender = ByteBufUtils.readUTF8String(buf);
    }

    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, Serializer.func_150696_a(this.msg));
        ByteBufUtils.writeUTF8String(buf, this.channel);
        ByteBufUtils.writeUTF8String(buf, this.sender);
    }

    public static class Handler implements IMessageHandler<MsgChatWrapperServer, IMessage> {
        public IMessage onMessage(MsgChatWrapperServer message, MessageContext ctx) {
            if (Talkative.soundNotices && !Minecraft.getMinecraft().thePlayer.getCommandSenderName().equals(message.sender) && !message.sender.equals("")) {
                Talkative.proxy.playSound(Talkative.soundName);
            }
            IChatComponent finalmsg = message.msg;
            if (message.channel.startsWith("#")) {
                PrattleChatEvent.ClientRecvChatEvent event = new PrattleChatEvent.ClientRecvChatEvent(finalmsg, message.sender, message.channel);
                if (MinecraftForge.EVENT_BUS.post((Event) event)) {
                    return null;
                }
                finalmsg = event.displayMsg;
            }
            GuiChatText guiChat = (GuiChatText) Minecraft.getMinecraft().ingameGUI.getChatGUI();
            guiChat.switchReceivingChannel(message.channel);
            ctx.getClientHandler().handleChat(new S02PacketChat(finalmsg));
            guiChat.switchReceivingChannel(Talkative.globalChat);
            return null;
        }
    }
}
