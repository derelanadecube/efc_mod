package com.existence.mod.escapeFromChernakov.chatModule.commands;

import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.server.Channel;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import com.existence.mod.escapeFromChernakov.chatModule.server.ReturnStatus;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;

import java.util.Iterator;

public class CommandChannel extends CommandBase {

    public String getCommandName() {
        return "channel";
    }

    public String getCommandUsage(ICommandSender sender) {
        return "/channel <help|flag|owner|op|deop> <channel> [params...]";
    }

    public void processCommand(ICommandSender sender, String[] data) {
        if (data.length < 1) {
            NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, this.getCommandUsage(sender)), sender);
        } else {
            if (data[0].equals("help")) {
                this.showHelp(sender, data);
            } else if (data[0].equals("flag")) {
                this.execFlag(sender, data);
            } else if (data[0].equals("owner")) {
                this.execOwner(sender, data);
            } else if (data[0].equals("op")) {
                this.execOp(sender, data);
            } else if (data[0].equals("deop")) {
                this.execDeop(sender, data);
            }

        }
    }

    private void execFlag(ICommandSender sender, String[] data) {
        if (data.length >= 2 && data[1].startsWith("#")) {
            Channel chan = ChannelHandler.INSTANCE.getChannel(data[1]);
            if (chan == null) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Channel " + data[1] + " not found."), sender);
            } else if (data.length == 2) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.JOIN + " : " + chan.get(Channel.ChanStat.JOIN)), sender);
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.PART + " : " + chan.get(Channel.ChanStat.PART)), sender);
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.GLOBAL + " : " + chan.get(Channel.ChanStat.GLOBAL)), sender);
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.PERMANENT + " : " + chan.get(Channel.ChanStat.PERMANENT)), sender);
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.FORCED + " : " + chan.get(Channel.ChanStat.FORCED)), sender);
            } else if (data.length != 4) {
                this.helpFlag(sender, data);
            } else if (!chan.isOp(sender.getCommandSenderName())) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "You are not OP or the owner of the channel."), sender);
            } else {
                Channel.ChanStat field;
                try {
                    field = Channel.ChanStat.valueOf(data[2].toUpperCase());
                } catch (Exception var7) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Unknown flag " + data[2].toUpperCase() + ". Possible flags are <JOIN|PART|GLOBAL|PERMANENT|FORCED>."), sender);
                    return;
                }

                if (sender instanceof EntityPlayerMP && field.isOP() && !((EntityPlayerMP) sender).mcServer.getConfigurationManager().func_152596_g(((EntityPlayerMP) sender).getGameProfile())) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Flag " + field + " can only be changed by a server admin."), sender);
                } else if (!data[3].toLowerCase().equals("true") && !data[3].toLowerCase().equals("false")) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Unknown value " + data[3].toUpperCase() + ". Possible values are <true|false>."), sender);
                } else {
                    boolean flag = Boolean.valueOf(data[3]).booleanValue();
                    chan.set(field, flag);
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "Flag " + field + " set to " + flag + " for channel " + data[1]), sender);
                    ChannelHandler.INSTANCE.markDirty();
                }
            }
        } else {
            this.helpFlag(sender, data);
        }
    }

    private void execOwner(ICommandSender sender, String[] data) {
        if (data.length >= 2 && data[1].startsWith("#")) {
            Channel chan = ChannelHandler.INSTANCE.getChannel(data[1]);
            if (chan == null) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Channel " + data[1] + " not found."), sender);
            } else if (data.length == 2) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "+ OWNER : " + chan.getOwner()), sender);
            } else if (data.length != 3) {
                this.helpFlag(sender, data);
            } else if (!chan.getOwner().equals(sender.getCommandSenderName()) && !sender.getCommandSenderName().equals("Server") && !sender.getCommandSenderName().equals("Rcon")) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "You are not the owner of the channel."), sender);
            } else {
                EntityPlayerMP target = CommandBase.getPlayer(sender, data[2]);
                if (target == null) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Player " + data[2] + " is not logged in."), sender);
                } else {
                    chan.setOwner(target.getGameProfile().getName());
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "Ownership for channel " + data[1] + " transfered to " + data[2]), sender);
                    ChannelHandler.INSTANCE.markDirty();
                }
            }
        } else {
            this.helpOwner(sender, data);
        }
    }

    private void execOp(ICommandSender sender, String[] data) {
        if (data.length >= 2 && data[1].startsWith("#")) {
            Channel chan = ChannelHandler.INSTANCE.getChannel(data[1]);
            if (chan == null) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Channel " + data[1] + " not found."), sender);
            } else if (data.length != 2) {
                if (data.length != 3) {
                    this.helpFlag(sender, data);
                } else if (!chan.isOp(sender.getCommandSenderName())) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "You are not OP or the owner of the channel."), sender);
                } else {
                    EntityPlayerMP target1 = CommandBase.getPlayer(sender, data[2]);
                    if (target1 == null) {
                        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Player " + data[2] + " is not logged in."), sender);
                    } else {
                        chan.addOp(target1.getGameProfile().getName());
                        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "Player " + data[2] + " added to the OP list for " + data[1]), sender);
                        ChannelHandler.INSTANCE.markDirty();
                    }
                }
            } else {
                if (chan.getOps().size() == 0) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "Not OP for this channel"), sender);
                }

                Iterator target = chan.getOps().iterator();

                while (target.hasNext()) {
                    String op = (String) target.next();
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "+ " + op), sender);
                }

            }
        } else {
            this.helpOp(sender, data);
        }
    }

    private void execDeop(ICommandSender sender, String[] data) {
        if (data.length >= 2 && data[1].startsWith("#")) {
            Channel chan = ChannelHandler.INSTANCE.getChannel(data[1]);
            if (chan == null) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Channel " + data[1] + " not found."), sender);
            } else if (data.length != 2) {
                if (data.length != 3) {
                    this.helpFlag(sender, data);
                } else if (!chan.isOp(sender.getCommandSenderName())) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "You are not OP or the owner of the channel."), sender);
                } else if (!chan.getOps().contains(data[2])) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Player " + data[2] + " is not OP for channel " + data[1]), sender);
                } else {
                    chan.rmOp(data[2]);
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "Player " + data[2] + " removed from the OP list for " + data[1]), sender);
                    ChannelHandler.INSTANCE.markDirty();
                }
            } else {
                if (chan.getOps().size() == 0) {
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "Not OP for this channel"), sender);
                }

                Iterator i$ = chan.getOps().iterator();

                while (i$.hasNext()) {
                    String op = (String) i$.next();
                    NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, "+ " + op), sender);
                }

            }
        } else {
            this.helpDeop(sender, data);
        }
    }

    private void helpFlag(ICommandSender sender, String[] data) {
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "/channel flag <channel> <flag> <true|false> is used to control the channel behavior."), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "/channel flag <channel> will display the current value for the flags."), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "Possible flags are :"), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.JOIN + " : " + Channel.ChanStat.JOIN.getDescription()), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.PART + " : " + Channel.ChanStat.PART.getDescription()), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.GLOBAL + " : " + Channel.ChanStat.GLOBAL.getDescription()), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.PERMANENT + " : " + Channel.ChanStat.PERMANENT.getDescription()), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "+ " + Channel.ChanStat.FORCED + " : " + Channel.ChanStat.FORCED.getDescription()), sender);
    }

    private void helpOwner(ICommandSender sender, String[] data) {
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "/channel owner <channel> <player> is used to transfer ownership."), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "/channel owner <channel> will display the current owner."), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "Only the current channel owner can use this command"), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "The owner of the channel behave like a super admin, unbound by some of the flags"), sender);
    }

    private void helpOp(ICommandSender sender, String[] data) {
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "/channel op <channel> <player> is used to give op to a player for this channel."), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "/channel op <channel> will display a list of ops."), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "Only ops and owners can use this command."), sender);
    }

    private void helpDeop(ICommandSender sender, String[] data) {
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "/channel deop <channel> <player> will revoke op power of the given user."), sender);
        NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "Only ops and owners can use this command."), sender);
    }

    private void showHelp(ICommandSender sender, String[] data) {
        if (data.length == 1) {
            NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "/channel is used to control channel parameters. For more information, please use /channel help <flag|owner|op|deop>."), sender);
        } else {
            if (data[1].equals("flag")) {
                this.helpFlag(sender, data);
            } else if (data[1].equals("owner")) {
                this.helpOwner(sender, data);
            } else if (data[1].equals("op")) {
                this.helpOp(sender, data);
            } else if (data[1].equals("deop")) {
                this.helpDeop(sender, data);
            } else {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, "Unknown subcommand, please user /channel help <flag|owner|op|deop>"), sender);
            }

        }
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
