package com.existence.mod.escapeFromChernakov.util;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import java.awt.*;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 15.05.2018
 * Time: 22:06
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
@SideOnly(Side.CLIENT)
public class UnicodeFontRenderer {

    private static Font font;
    public static UnicodeFont FONT_48;
    public static UnicodeFont FONT_42;
    public static UnicodeFont FONT_32;
    public static UnicodeFont FONT_16;
    public static UnicodeFont FONT_18;
    public static UnicodeFont FONT_14;

    private static Color COLOR = new Color(176, 175, 172, 255);

    public static void init() throws SlickException {
        try {
            font = Font.createFont(Font.PLAIN, Minecraft.getMinecraft().getResourceManager().getResource(new ResourceLocation("escapefromchernakov", "font/BenderBlack.ttf")).getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
        FONT_48 = initFont(font, 48.0F);
        FONT_42 = initFont(font, 42.0F);
        FONT_32 = initFont(font, 32.0F);
        FONT_18 = initFont(font, 18.0F);
        FONT_16 = initFont(font, 16.0F);
        FONT_14 = initFont(font, 14.0F);
    }

    public static void renderString(String text, int i, int j) {
        //GL11.glPushMatrix();
        //GL11.glScalef(0.25F, 0.25F, 0.25F);
        //GL11.glEnable(GL11.GL_BLEND);
        //GL11.glDrawBuffer(GL11.GL_BACK);
        //FONT_32.drawString(i * 4F, j * 4F + 7 , text, COLOR);
        //GL11.glDisable(GL11.GL_BLEND);
        //GL11.glScalef(0.5F, 0.5F, 0.5F);
        //GL11.glPopMatrix();

        GL11.glPushMatrix();
        GL11.glScalef(0.25F, 0.25F, 0.25F);
        GL11.glEnable(GL11.GL_BLEND);

        FONT_32.drawString(i * 4F, j * 4F + 7, text, COLOR);

        GL11.glDisable(GL11.GL_BLEND);
        GL11.glScalef(2.0F, 2.0F, 2.0F);
        GL11.glPopMatrix();
    }

    public static void renderStringWithColorAndFont(String text, int i, int j, Color color, org.newdawn.slick.Font font) {
        GL11.glPushMatrix();
        GL11.glScalef(0.25F, 0.25F, 0.25F);
        GL11.glEnable(GL11.GL_BLEND);

        FONT_32.drawString(i * 4F, j * 4F + 7, text, color);

        GL11.glDisable(GL11.GL_BLEND);
        GL11.glScalef(2.0F, 2.0F, 2.0F);
        GL11.glPopMatrix();
    }


    private static UnicodeFont initFont(Font fontToLoad, float size) throws SlickException {
        UnicodeFont font = new UnicodeFont(fontToLoad.deriveFont(size));
        font.addAsciiGlyphs();
        font.addGlyphs(32, 1200);
        font.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
        font.loadGlyphs();
        return font;
    }
}
