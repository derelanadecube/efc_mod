package com.existence.mod.escapeFromChernakov.util;

import com.existence.mod.escapeFromChernakov.mainModule.EFCReference;
import cpw.mods.fml.relauncher.FMLRelaunchLog;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;


/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 08.05.2018
 * Time: 14:34
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class EFCLogger {

    private static FMLRelaunchLog coreLog = FMLRelaunchLog.log;

    public static void log(Level level, String stringOutput, Object... data) {
        coreLog.log(EFCReference.MOD_ID, level, stringOutput, data);
    }

    public static void log(Level level, Throwable ex, String stringOutput, Object... data) {
        coreLog.log(EFCReference.MOD_ID, level, ex, stringOutput, data);
    }

    public static void bigWarning(String stringOutput, Object... data) {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        log(Level.WARN, "****************************************");
        log(Level.WARN, "* " + stringOutput, data);
        for (int i = 2; i < 8 && i < trace.length; i++) {
            log(Level.WARN, "*  at %s%s", trace[i].toString(), i == 7 ? "..." : "");
        }
        log(Level.WARN, "****************************************");
    }

    public static void warn(String stringOutput, Object... data) {
        log(Level.WARN, stringOutput, data);
    }

    public static void info(String stringOutput, Object... data) {
        log(Level.INFO, stringOutput, data);
    }

    public static void debug(String stringOutput, Object... data) {
        log(Level.DEBUG, stringOutput, data);
    }

    public static void error(String stringOutput, Object... data) {
        log(Level.ERROR, stringOutput, data);
    }

    public static void error(String stringOutput, Throwable t, Object... data) {
        log(Level.ERROR, t, stringOutput, data);
    }

    public static void fatal(String stringOutput, Object... data) {
        log(Level.FATAL, stringOutput, data);
    }

    public static void fatal(String stringOutput, Throwable t, Object... data) {
        log(Level.FATAL, t, stringOutput, data);
    }

    public static Logger getLogger() {
        return coreLog.getLogger();
    }
}
