package com.existence.mod.escapeFromChernakov.util;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 10.05.2018
 * Time: 20:49
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class LibResources {

    public static final String PREFIX_LANG = "/assets/efc/lang/";
    public static final String PREFIX_SHADER = "/assets/escapefromchernakov/shader/";

    public static final String SHADER_FILM_GRAIN_FRAG = PREFIX_SHADER + "shader.frag";
}
