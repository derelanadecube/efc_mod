package com.existence.mod.escapeFromChernakov.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 15.05.2018
 * Time: 11:37
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class TextureMap {

    private static Map textures = new HashMap();
    private static DynamicTexture empty = new DynamicTexture(1, 1);
    public static String res;

    public static void add(String name, DynamicTexture texture) {
        textures.put(name, texture);
    }

    public static DynamicTexture get(String name) {
        if (textures.containsKey(name)) {
            DynamicTexture texture = (DynamicTexture) textures.get(name);
            if (!texture.isAllocate) {
                texture.allocateTexture();
            }

            return (DynamicTexture) textures.get(name);
        } else {
            if (!empty.isAllocate) {
                empty.allocateTexture();
            }

            return empty;
        }
    }

    public static boolean contains(String tex) {
        return textures.containsKey(tex);
    }

    public static void load(String s) {
        res = s;
        File f = new File(s);
        TextureMap.LoadModel thread = new TextureMap.LoadModel(f);
        thread.setDaemon(true);
        thread.setName("Loading texture thread");
        thread.start();
    }

    public static void reload() {
        textures.clear();
        load(res);
    }


    static class LoadModel extends Thread {

        private File textureFile;

        public LoadModel(File textureFile) {
            this.textureFile = textureFile;
        }

        public void run() {
            try {
                File[] e = (new File(TextureMap.res)).listFiles(new FileFilter() {
                    public boolean accept(File file) {
                        return file.isDirectory();
                    }
                });
                File[] var2 = e;
                int var3 = e.length;

                for (int var4 = 0; var4 < var3; ++var4) {
                    File dir = var2[var4];
                    this.loadFromDir(dir);
                }

                this.loadFromDir(new File(TextureMap.res));
            } catch (Exception var6) {
                var6.printStackTrace();
            }
        }

        public void loadFromDir(File diectory) throws IOException {
            File[] var2 = diectory.listFiles();
            int var3 = var2.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                File file = var2[var4];
                String name = file.getName();
                if (name.endsWith(".png")) {
                    name = name.substring(0, name.length() - 4);
                    BufferedImage buf = null;
                    buf = ImageIO.read(file);
                    DynamicTexture texture = new DynamicTexture(buf);
                    TextureMap.textures.put(name, texture);
                }
            }
        }
    }
}