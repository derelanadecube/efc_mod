package com.existence.mod.escapeFromChernakov.util;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.AbstractTexture;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.resources.IResourceManager;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 15.05.2018
 * Time: 11:36
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
@SideOnly(Side.CLIENT)
public class DynamicTexture extends AbstractTexture {

    private final int[] dynamicTextureData;
    private final int width;
    private final int height;
    private static final String __OBFID = "CL_00001048";
    public boolean isAllocate;


    public DynamicTexture(BufferedImage bufferedImage) {
        this(bufferedImage.getWidth(), bufferedImage.getHeight());
        bufferedImage.getRGB(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), this.dynamicTextureData, 0, bufferedImage.getWidth());
    }

    public DynamicTexture(int p_i1271_1_, int p_i1271_2_) {
        this.isAllocate = false;
        this.width = p_i1271_1_;
        this.height = p_i1271_2_;
        this.dynamicTextureData = new int[p_i1271_1_ * p_i1271_2_];
    }

    public void allocateTexture() {
        if (!this.isAllocate) {
            TextureUtil.allocateTexture(this.getGlTextureId(), this.width, this.height);
            this.updateDynamicTexture();
            this.isAllocate = true;
        }

    }

    public void loadTexture(IResourceManager p_110551_1_) throws IOException {
    }

    public void updateDynamicTexture() {
        TextureUtil.uploadTexture(this.getGlTextureId(), this.dynamicTextureData, this.width, this.height);
    }

    public int[] getTextureData() {
        return this.dynamicTextureData;
    }
}